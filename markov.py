import numpy as np
import math


class Markov:
    def __init__(self, inMarkovChain):
        self.setMatrix(inMarkovChain)

    def setMatrix(self, inMarkovChain):
        if inMarkovChain is None:
            return

        self.baseMarkovChain = inMarkovChain
        # get identity matrice
        self.mainMarkovChain = np.matmul(
            self.baseMarkovChain, np.linalg.inv(self.baseMarkovChain))

    def reset(self):
        self.setMatrix(self.baseMarkovChain)

    def advanceOneYear(self):
        self.advanceNYears(1)

    def advanceNYears(self, N):
        if self.baseMarkovChain is None or self.mainMarkovChain is None or N < 0:
            return

        nYearMarkovChain = np.linalg.matrix_power(
            self.baseMarkovChain, N)  # M^n

        # AB != BA
        # M'n+3 = M' * M^3 = M' * M * M * M with M' = M * M ... M
        self.mainMarkovChain = np.matmul(
            self.mainMarkovChain, nYearMarkovChain)

    def simulate(self, pop):
        if pop is None:
            return

        return np.matmul(pop, self.mainMarkovChain)

    def getNyearsMarkovChain(self):
        return self.mainMarkovChain
