from markov import *
import matplotlib.pyplot as plt

print("# Question 8")

basechain = np.array(
    [[0.92, 0.03, 0.05], [0.02, 0.92, 0.06], [0.06, 0.05, 0.89]])

basechainmatrixidentity = np.matmul(basechain, np.linalg.inv(
    basechain))  # or np.identity(3.0)

pop = np.array([100, 200, 300])


m = Markov(basechain)

for i in range(0, 10 + 1):
    print("Year ", i)
    print(m.simulate(pop))
    m.advanceOneYear()

m.reset()

# Question 9
print("# Question 9")

t = 1
offset = 2

popTyears = None
popT2years = None
popT2yearsFromTyears = None

m.advanceNYears(t)
popTyears = m.simulate(pop)
print("T =", t, ":", popTyears)

m.advanceNYears(offset)
popT2years = m.simulate(pop)
print("T =", t + offset, ":", popT2years)

m.reset()

m.advanceNYears(2)
popT2yearsFromTyears = m.simulate(popTyears)
print("T =", t + offset, ":", popT2yearsFromTyears)

m.reset()

# Question 10, 11, 12

cities = ["Angers", "Nantes", "Paris"]
datasDic = {}

years = 100

datasDic[cities[0]] = []
datasDic[cities[1]] = []
datasDic[cities[2]] = []
datasDic["Years"] = []
datasDic["StationaryValue"] = []

for i in range(years):

    t = m.simulate(pop)

    datasDic["Angers"].append(t[0])
    datasDic["Nantes"].append(t[1])
    datasDic["Paris"].append(t[2])
    datasDic["Years"].append(i)

    m.advanceOneYear()

print(m.getNyearsMarkovChain())
print(np.matmul(pop, m.getNyearsMarkovChain()))

fig, ax = plt.subplots(figsize=(8, 4))

for i, cityName in enumerate(cities):
    line = plt.plot(datasDic["Years"],
                    datasDic[cityName],
                    label=cityName)

# Question 13

# solve S*basechain=S to get the stationary distribution
# such that the sum of each row of column of S should be equal to 1
# then, multiply the stationary distribution by the total population

# if doubly stocastic then, (x,y,z) = (1/n,1/n,1/n) with n = degre of the npulet (3 here)

""""
So, We want:

(x,y,z)*M = (x,y,z)
Because, if the markov chain tends to something, it will be the something that when multiplied by, doen't
change the markov chain.

M = [[x1, y1, z2],
     [x2, y2, z2],
     [x3, y3, z3]]

x1*x + x1*y + z1*z = x
x2*x + y1*y + z2*z = y
x3*x + y3*y + z3*z = z

which means

(x,y,z) * (M - I3) = 0

there is also the eignvector trick

"""""

if np.linalg.det(basechain) != 0:
    # tmp = np.transpose(basechain)
    # values, vectors = np.linalg.eig(tmp)
    # # get the eignvector which has eignvalue 1 (U - 1 < 1e-8)
    # stationary = np.array(
    #     vectors[:, np.where(np.abs(values - 1.) < 1e-8)[0][0]].flat)
    # stationary = stationary / np.sum(stationary)
    # print("Stationary distribution of the population : ", stationary)

    tmp = basechain  # np.subtract(basechain, basechainmatrixidentity)
    stationary = np.linalg.solve(tmp, np.array([1, 1, 1]))  # 0 0 0
    stationary = stationary / np.sum(stationary)

    stationary *= sum(pop)
    print(stationary)
    for i in range(years):
        datasDic["StationaryValue"].append(stationary.item(0).real)

    line = plt.plot(datasDic["Years"],
                    datasDic["StationaryValue"],
                    label="Stationary Value")


ax.grid(True)
ax.legend(loc='right')
ax.set_title('Evolution of the population in each city of the country')
ax.set_xlabel('Years')
ax.set_ylabel('Population as a function of time')

plt.savefig("population_evolution.svg")
